import pandas as pd

auc_f = snakemake.input['auc']
out_f = snakemake.output['out']

# Read each auc_f and append into a df, then sort by trait, model and adjusted columns and save

comb = None

for f in auc_f:
    df = pd.read_csv(f, sep=' ')
   
    if comb is None:
        comb = df
        
    else:
        comb = comb.append(df)
       

comb.sort_values(by=['trait', 'model', 'adjusted'], inplace=True)

comb.to_csv(out_f, index=False, sep = ' ')
