import os
import pathlib 

# set global variable to save metafiles, needs to be done before calling open_bgen
meta_dir = snakemake.params['meta_dir']

os.environ["BGEN_READER_CACHE_HOME"] = meta_dir

# get name and path to bgen file

bgen = snakemake.input['bgen']
# get dir with bgen file
bgen_dir = os.path.dirname(bgen)
# get basename of bgen file
bgen_name = os.path.basename(bgen)

# change working directory to dir with bgen files (recommended by Carl from  bgen_reader-py) 
os.chdir(bgen_dir)

from bgen_reader._metafile import infer_metafile_filepath
metadata_file = infer_metafile_filepath(pathlib.Path(bgen_name))
print(metadata_file)


from bgen_reader import open_bgen
bgen_open = open_bgen(bgen_name, verbose=True)

del bgen_open
