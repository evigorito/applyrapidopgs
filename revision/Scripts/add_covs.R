## Add additional covariates to scores

library(data.table)


## read snakemake input and output files:
score.f <- snakemake@input[['score']]
pheno.f <- snakemake@input[['eid']]
out.f <- snakemake@output[['out']]
cov_codes <- snakemake@params[['cov_codes']]
cov_names <- snakemake@params[['cov_names']]

## Process data

score <- fread(score.f)

## get f.eid and covs

dt <- fread(pheno.f, header=T, select=c("eid", cov_codes),
             col.names = c("f.eid", cov_names))

if(any(cov_names %in% names(score))){
    n <- names(score)[names(score) %in% cov_names]
    score[, (n) := NULL]
}
    write.table(merge(dt, score, by="f.eid"), out.f, row.names=F)

