# Given  the sub-scores computed in rule run_subscores for a given pgs, this code sum them variantes per sample and returns a table with columns the score and sample id.

# system imports
import os
import pandas as pd
import sys
import numpy as np

# import local functions
sys.path.insert(0, '/home/ev250/myPypacks/pyrunPGS/pyrunPGS')
import geno_pgs

# get Snakefile inputs:
scores_files = snakemake.input['scores']
sample_file = snakemake.input['sample_bgen']
out_file = snakemake.output['scores']

geno_pgs.sum_subscores(scores_files, out_file, sample_file)
