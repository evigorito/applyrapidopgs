#############################################################################
## Snakefile for evaluating PGS to UKBB data; revision for Guille's paper 
#############################################################################

shell.prefix("source ~/.bashrc; ") 

configfile: "config.yaml" 

localrules: all

import os
import os.path
import re
import pandas as pd
import gzip
import glob
import re



subworkflow covid:
    workdir:
        "/home/ev250/ukbb_covid19"

subworkflow rapido:
    workdir:
        "/home/ev250/rapidoPGS"

home_covid = vars(vars(workflow)['_subworkflows']['covid'])['_workdir']
    
home_rapido = vars(vars(workflow)['_subworkflows']['rapido'])['_workdir']

# Functions from home_rapido

def file_basename_dic(files):
    """Giving a list of files make a dictionary with keys basename and value full name"""
    dic={}
    for f in files:
        dic[os.path.basename(f)]=f
    return(dic)
 

def pgs_chrom_n(id, model_dir, chrom="CHR"):
    """Make a dictionary with key a PGS ldpred2 file name  and values a pandas series. Each series has keys chromosomes and values the number of variants for it. id is the basename of the model, model_dir is the directory where the model is saved and chrom is the column in model for chromosome"""
    dic={}
    df = pd.read_csv(model_dir + "/" + id , sep=' ')
    dic[id]=df[chrom].value_counts()
    return(dic)


def break_chrom(d, b=10**5):
    """given an element from a dictionary with key PGS name and value a pandas series with keys chromosome and values total number of variants per chromosome (created with pgs_chrom_n,create a list with the end line to read from PGS file for each job"""
    dic={}
    keys=d.keys()
    for key in keys:
        v=[x for x in range(b, d[key], b)]
        v.append(d[key])
        dic[key] = v
    return(dic)


# local variables
chroms=[x+1 for x in range(22)]

all_models={**file_basename_dic(glob.glob(config['pgs'] + "/*")), **file_basename_dic(glob.glob(config['pgs'] + "/*")) } 

r=re.compile("RA_int.*")
RA_int=list(filter(r.match, all_models.keys()))

          
model_files_all = [os.path.basename(x) for x in os.listdir(config['pgs'])] # was model_files, relabelled to avoid Guillermo's change of timestamp for rapido models (18/3), only RA_int models are necessary to run

model_files = list(filter(r.match, model_files_all))
model_qc_dir = config['output_dir'] + "/model_QC"
model_qc = ["pgs_ukbb_" + x for x in model_files]
model_trait = {model: model.split("_")[0] for model in all_models.keys()}
cont_traits = ['BMI', 'Height']
case_ctl = {k: v for(k, v) in model_trait.items() if v not in cont_traits}
cont_dic = {k: v for(k, v) in model_trait.items() if v in cont_traits}
pgs_dic_counts={s:pgs_chrom_n(s, model_qc_dir)[s] for s in model_qc}

cpu_val={str(c):int(c) for c in [2,4]}
rule all:
    input:
        # expand(config['out_tmp'] + "/meta_data_bgen_py/metafile/ukb_imp_chr{chrom}_v3.bgen.metadata2.mmm", chrom=chroms),
        # expand(config['output_dir'] + "/model_QC/failed_{model}", model=all_models.keys()),
        # expand(config['output_dir'] + "/model_QC/pgs_ukbb_{model}", model=all_models.keys())  
        # [config["out_tmp"] + "/test_score/{model}.{chrom}.{breaks}.{cpu}.txt".format(model=m, chrom = str(bk), breaks = str(v), cpu = c) for m in all_models.keys() for bk in [20] for v in [10**4] for c in cpu_val.keys()]
        # [config["out_tmp"] + "/ukbb_score/{model}/{model}.{chrom}.{breaks}.txt".format(model=m, chrom = str(bk), breaks = str(v)) for m in model_files for bk in break_chrom(d=pgs_dic_counts["pgs_ukbb_" + m]).keys() for v in break_chrom(d=pgs_dic_counts["pgs_ukbb_" + m])[bk]],
        # expand(config["out_tmp"] + "/ukbb_score/{model}/{model}.{chrom}.{breaks}.txt", model=all_models.keys(), chrom=[1, 20] , breaks=[10**5, 10**5])
        # expand(config['out_tmp'] + "/ukbb_score/{model}/{model}.total.txt" , model=model_files),
        # expand(config['out_tmp'] + '/test_score/{model}.{cpu}.total.txt', model=all_models.keys(), cpu=cpu_val.keys())
        # expand(config['out_tmp'] + "/ukbb_score/ind_subset/{model}.score.txt", model =all_models.keys()),
        # expand(config['out_tmp'] + "/ukbb_score/ind_subset/{model}.score.covs.txt", model =all_models.keys())
        # expand(config['out_tmp'] + "/benchmark/{model}.auc_r2l_case_ctl.txt", model = case_ctl.keys())
        config['output_dir'] + "/benchmark/auc_r2l_case_ctl.txt",
        # config['output_dir'] + "/benchmark/r2_cont.txt"


rule read_bgen_py:
    """To read bgen files in Python I need to call open_reader from bgen_reader module. To speed up access open_reader creates a metadata file and saves it which allows quick access to the bgen file in future calls. This is done once per file, unless samples changes. I specify a dir to saave the metadata files in meta_dir"""
    input:
        bgen=config['geno_dir'] + "/ukb_imp_chr{chrom}_v3.bgen",
    params:
        meta_dir=config['out_tmp'] + "/meta_data_bgen_py",
    output:
        out=config['out_tmp'] + "/meta_data_bgen_py/metafile/ukb_imp_chr{chrom}_v3.bgen.metadata2.mmm"
    script:
        "Scripts/meta_bgen.py"

# rule qc_score:
#     """Given a PGS score, check for SNPs above info threshold. Flag flipped alleles in PGS file relative to UKBB. Flag multiallelic SNPs and keep SNP id to match bgen file. From ukbb:/ukb_mfi_chr{chrom}_v3.txt lists the minor allele frequency and info score for each of the markers in the imputed data, calculated using QCTOOL. The order of markers in these files is not guaranteed to be the same as the BGEN files. """
#     input:
#         pgs=lambda wildcards: all_models[wildcards.model],
#         bbsnps=expand(config['geno_dir'] + "/ukb_mfi_chr{chrom}_v3.txt", chrom=[x+1 for x in range(22)])
#     params:
#         info=0.3,
#         cols=["CHR", "BP", "REF", "ALT", "weight"],
#         out_dir=config['output_dir'] + "/model_QC"
#     output:
#         failed=config['output_dir'] + "/model_QC/failed_{model}",
#         pgs=config['output_dir'] + "/model_QC/pgs_ukbb_{model}"
#     script:
#         "Scripts/SNPs_QC.py"
        
rule test_efficiency:
    """try to improve efficiency for compPGS.R script. Use a python version to see if I can improve. When reading bgen files in Python a metadata file needs to be saved which allows quick access to bgen file. This is done once per file, unless samples changes."""
    input:
        bgen=config['geno_dir'] + "/ukb_imp_chr{chrom}_v3.bgen",
        pgs=config['output_dir'] + "/model_QC/pgs_ukbb_{model}",
    threads:
        lambda wildcards: cpu_val[wildcards.cpu]
    params:
        genome_build=37,
        chunk=10**4,
        sub_chunk=2*10**3,
        cols=["CHR", "BP",  "weight"],
        chrom="{chrom}",
        meta_dir=config['out_tmp'] + "/meta_data_bgen_py"
    wildcard_constraints:
        chrom="\d+",
        breaks="\d+",
        cpu="\d+"
    output:
        out=config["out_tmp"] + "/test_score/{model}.{chrom}.{breaks}.{cpu}.txt"
    script:
        "Scripts/test_eff.py"


rule run_subscores:
    """given a pgs score split by 100K variants and compute sub_score. Based on previous tests I decided to run jobs of 100K variants each using 2 cores"""
    input:
        bgen=config['geno_dir'] + "/ukb_imp_chr{chrom}_v3.bgen",
        pgs=config['output_dir'] + "/model_QC/pgs_ukbb_{model}",
    threads:
        4
    params:
        genome_build=37,
        chunk=10**5,
        sub_chunk=2*10**3,
        cols=["CHR", "BP",  "weight"],
        chrom="{chrom}",
        meta_dir=config['out_tmp'] + "/meta_data_bgen_py"
    wildcard_constraints:
        chrom="\d+",
        breaks="\d+",
        cpu="\d+"
    output:
        out=temp(config["out_tmp"] + "/ukbb_score/{model}/{model}.{chrom}.{breaks}.txt")
    script:
        "Scripts/test_eff.py"    
        
        
rule sum_subscores:
    """Given sub scores processed in chunks for a given pgs, add them together and add sample id. All sample files are the same, I just selected chrom 1 for simplicity."""
    input:
        scores=lambda wildcards: [config['out_tmp'] + "/ukbb_score/" + wildcards.model + "/" + wildcards.model + ".{chrom}.{breaks}.txt".format(chrom=str(bk), breaks=str(v)) for bk 
in break_chrom(d=pgs_dic_counts["pgs_ukbb_" + wildcards.model]).keys() for v in break_chrom(d=pgs_dic_counts["pgs_ukbb_" + wildcards.model])[bk]],
        sample_bgen=config['pheno_dir'] + "/samplefiles/ukb30931_imp_chr1_v3_s487296.sample"
    wildcard_constraints:
        chrom="\d+",
        breaks="\d+",
    output:
        scores=config['out_tmp'] + "/ukbb_score/{model}/{model}.total.txt"
    script:
        "Scripts/sum_subscores.py"
        

rule subset_score:
    """Select relevant individuals to report score, same as in ld2pred paper, https://github.com/privefl/simus-PRS/blob/master/paper3-SCT/code_real/ trait is modified to keep compatibility to the names given by Guillermo in previous version: AST -> Asthma, HEIGHT -> Height"""
    input:
        score=config['out_tmp'] + "/ukbb_score/{model}/{model}.total.txt",
        rel_ind=config["pheno_dir"] + "/keyfile/ukb30931_rel_s488264.dat",
        eid=config['eid_pheno']
    params:
        trait=lambda wildcards:  wildcards.model.split("_")[0].upper().replace('ASTHMA','AST')
    output:
        score=config['out_tmp'] + "/ukbb_score/ind_subset/{model}.score.txt"
    script:
        home_rapido + "/Scripts/{params.trait}.subset.R"

rule add_covars:
    """Add age (21003-0.0), sex(21001-0.0) (if not already) and PCs (22009-0.1-40) for further adjustement when comparing PGS computed from different methods and traits"""
    input:
        score=config['out_tmp'] + "/ukbb_score/ind_subset/{model}.score.txt",
        eid=config['eid_pheno']
    params:
        cov_codes=["21003-0.0", "22001-0.0"] + [ "22009-0."  + str(x + 1) for x in range(40)],
        cov_names=["age", "sex"] + ["PC" + str(x + 1) for x in range(40)]
    output:
        out=config['out_tmp'] + "/ukbb_score/ind_subset/{model}.score.covs.txt"
    script:
        "Scripts/add_covs.R"
        

rule pgs_auc:
    """Computing adjusted AUC takes long time, I introduce this rule to avoid recomputation for the models that are already run"""
    input:
        score=config['out_tmp'] + "/ukbb_score/ind_subset/{model}.score.covs.txt"
    params:
        cov_names=["age", "sex"] + ["PC" + str(x + 1) for x in range(40)],
        trait=lambda wildcards: case_ctl[wildcards.model],
    threads:
        1
    output:
        out=config['out_tmp'] + "/benchmark/{model}.auc_r2l_case_ctl.txt"
    script:
        "Scripts/auc_pgs.R"

rule merge_auc:
    """Combine pgs_auc from all models into a table"""
    input:
        auc=expand(config['out_tmp'] + "/benchmark/{model}.auc_r2l_case_ctl.txt", model= case_ctl.keys())
    output:
        out=config['output_dir'] + "/benchmark/auc_r2l_case_ctl.txt"
    script:
        "Scripts/combine_auc.py"

# rule pgs_metrics:
#     """Compute various metrics to compare pgs. This rule was substituted by pgs_auc, merge_auc and pgs_r2cont to avoid recomputing of adjusted AUC because it takes long time."""
#     input:
#         score=expand(config['out_tmp'] + "/ukbb_score/ind_subset/{model}.score.covs.txt", model= all_models.keys())
#     params:
#         cov_names=["age", "sex"] + ["PC" + str(x + 1) for x in range(40)],
#         cont_traits=['BMI', 'Height'],
#         out_dir=config['output_dir'] + "/benchmark"
#     threads:
#         16
#     output:
#         config['output_dir'] + "/benchmark/auc_r2l_case_ctl.txt"
#     script:
#         "Scripts/metrics_pgs.R"

rule pgs_r2cont:
    """Compute r2 for contimuous traits, all together"""
    input:
        score=expand(config['out_tmp'] + "/ukbb_score/ind_subset/{model}.score.covs.txt", model= cont_dic.keys())
    params:
        cov_names=["age", "sex"] + ["PC" + str(x + 1) for x in range(40)]
    output:
        out=config['output_dir'] + "/benchmark/r2_cont.txt"
    script:
        "Scripts/r2_pgs.R"
    
        
## snakemake  -k -j 500 --cluster-config cpu.json --cluster "sbatch -A {cluster.account} -p {cluster.partition}  -c {cluster.cpus-per-task}   -t {cluster.time} --output {cluster.error} -J {cluster.job} "
