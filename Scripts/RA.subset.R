library(data.table)

## subset samples as in ld2pred paper https://github.com/privefl/simus-PRS/blob/master/paper3-SCT/code_real/02-SCT-RA.R


## read snakemake input and output files:

score.f <- snakemake@input[['score']]
rel_ind.f <- snakemake@input[['rel_ind']]
pheno.f <- snakemake@input[['eid']]
out.f <- snakemake@output[['score']]

score <- fread(score.f)

## get f.eid , sex and caucasian (codes 22001-0.0 22006-0.0)

dt0 <- fread(pheno.f, header=T, select=c("eid", "22006-0.0"),
             col.names = c("f.eid", "is_caucasian"))

## add related individual info to dt0

rel_ind <- dt0$f.eid %in% fread(rel_ind.f)$ID2

dt0[ , is_rel2 := rel_ind]

## get no cancer disease fields
df_illness <- fread(pheno.f, select = c(paste0("20002-0.", 0:28),
                                     paste0("20002-1.", 0:28),
                                     paste0("20002-2.", 0:28)))
df_ICD10 <- fread(pheno.f, select = c(paste0("40001-", 0:2, ".0"),
                                   paste0("40002-0.", 0:13),
                                   paste0("40002-1.", 0:13),
                                   paste0("40002-2.", 0:13),
                                   paste0("41202-0.", 0:379),
                                   paste0("41204-0.", 0:434)))
ind_RA <- sort(unique(unlist(c(
  lapply(df_illness, function(x) which(x == 1464)),
  lapply(df_ICD10,   function(x) which(substr(x, 1, 3) %in% c("M05", "M06")))
))))

## exclude other muscular conditions
ind_muscu <- sort(unique(unlist(c(
  lapply(df_illness, function(x) which(x %in% c(1295, 1464:1467, 1477, 1538))),
  lapply(df_ICD10,   function(x) which(substr(x, 1, 1) == "M"))
))))

## select cases and controls
y <- rep(0, nrow(dt0)); y[ind_muscu] <- NA; y[ind_RA] <- 1

## add phenotype to dt0
dt0[, RA:=y]


## merge scores with dt0

score <- merge(dt0, score, by="f.eid")

## select scores for un-related, caucasian excluding other resp diseases


score <- score[is_caucasian ==1 & !is_rel2 & !is.na(RA), ]


write.table(score, out.f, row.names=F)
