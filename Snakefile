##############################################################
## Snakefile for evaluating PGS to UKBB data 
##############################################################

shell.prefix("source ~/.bashrc; ") 

configfile: "config.yaml" 

localrules: all

import os
import os.path
import re
import pandas as pd
import gzip
import glob



subworkflow covid:
    workdir:
        "/home/ev250/ukbb_covid19"

home_covid = vars(vars(workflow)['_subworkflows']['covid'])['_workdir']


def file_basename_dic(files):
    """Giving a list of files make a dictionary with keys basename and value full name"""
    dic={}
    for f in files:
        dic[os.path.basename(f)]=f
    return(dic)
 

def pgs_chrom_n(id, model_dir, chrom="CHR"):
    """Make a dictionary with key a PGS ldpred2 file name  and values a pandas series. Each series has keys chromosomes and values the number of variants for it. id is the basename of the model, model_dir is the directory where the model is saved and chrom is the column in model for chromosome"""
    dic={}
    df = pd.read_csv(model_dir + "/" + id , sep=' ')
    dic[id]=df[chrom].value_counts()
    return(dic)


def break_chrom(d, b=1000):
    """given an element from a dictionary with key PGS name and value a pandas series with keys chromosome and values total number of variants per chromosome (created with pgs_chrom_n,create a list with the end line to read from PGS file for each job"""
    dic={}
    keys=d.keys()
    for key in keys:
        v=[x for x in range(b, d[key], b)]
        v.append(d[key])
        dic[key] = v
    return(dic)

all_models={**file_basename_dic(glob.glob(config['pgs'] + "/nofilt/*")), **file_basename_dic(glob.glob(config['pgs'] + "/qcfilt/*")) }

 
# model_files_old = [os.path.basename(x) for x in os.listdir(config['pgs'] + "/nofilt")]
model_files_new = [os.path.basename(x) for x in os.listdir(config['pgs'] + "/qcfilt")]
# r=re.compile("RA.*RapidoPGSsingle2.*|PRCA.*RapidoPGSsingle2.*")

# model_files = list(filter(r.match, model_files_new))
model_files=model_files_new
model_qc_dir = config['output_dir'] + "/model_QC"
model_qc = ["pgs_ukbb_" + x for x in model_files]
# pgs_dic_counts={s:pgs_chrom_n(s, model_qc_dir)[s] for s in model_qc} 


rule all:
    input:
        # expand(config['output_dir'] + "/model_QC/failed_{model}", model = all_models.keys()),
        # [config["out_tmp"] + "/ukbb_scores/{model}/{model}.{chrom}.{breaks}.txt".format(model=s, chrom=str(bk), breaks=str(v)) for s in ["AST_Demenais_29273806_1_LDpred2_auto_1e4_4000_nofilt.full.model.1.10.txt"] for bk breaks.keys() for v in breaks[bk]]
        # [config["out_tmp"] + "/ukbb_score/{model}/{model}.{chrom}.{breaks}.txt".format(model=s, chrom=str(bk), breaks=str(v)) for s in model_files for bk in break_chrom(d=pgs_dic_counts["pgs_ukbb_" + s]).keys() for v in break_chrom(d=pgs_dic_counts["pgs_ukbb_" + s])[bk]],
        # expand(config['out_tmp'] + "/ukbb_score/{model}/{model}.total.txt", model=model_files),
        # expand(config['out_tmp'] + "/ukbb_score/ind_subset/{model}.score.txt", model=model_files)
        # "Scripts/report_scores.pdf" 
        config['output_dir'] +  "/input_test/T2D_Scott_28566273_1_qcfilt_RapidoPGSsingle_prior02.full.model.chrom1.100var.rds", config['output_dir'] +  "/input_test/T2D_Scott_28566273_1_qcfilt_RapidoPGSsingle_prior02.full.model.chrom1.100var.info.txt"


rule score_qc:
    """Given a PGS score, check for SNPs above info threshold and align alleles and weight in PGS file to UKBB if necessary."""
    input:
        pgs=lambda wildcards: all_models[wildcards.model],
        bbsnps=expand(config['geno_dir'] + "/ukb_mfi_chr{chrom}_v3.txt", chrom=[x+1 for x in range(22)])
    params:
        info=0.3,
        cols=["CHR", "BP", "REF", "ALT", "SNPID"],
        out_dir=config['output_dir'] + "/model_QC"
    output:
        failed=config['output_dir'] + "/model_QC/failed_{model}",
        pgs=config['output_dir'] + "/model_QC/pgs_ukbb_{model}"
    script:
        "Scripts/SNPs_QC.R"


rule comput_PGS:
    """Compute PGS calculated by Guillermo for UKBB. I split PGS file and paralelise per chromosome up to 1000 (chunk parameter , same as b in breaks_chrom function) variants in each go."""
    input:
        bgen=config['geno_dir'] + "/ukb_imp_chr{chrom}_v3.bgen",
        pgs=config['output_dir'] + "/model_QC/pgs_ukbb_{model}",
        sample_bgen=config['pheno_dir'] + "/samplefiles/ukb30931_imp_chr{chrom}_v3_s487296.sample"
    params:
        genome_build=37,
        chunk=1000,
        cols=["CHR", "BP", "REF", "ALT", "rsid", "weight"]
    wildcard_constraints:
        chrom="\d+",
        breaks="\d+"
    output:
        out=temp(config["out_tmp"] + "/ukbb_score/{model}/{model}.{chrom}.{breaks}.txt")
    script:
        "Scripts/compPGS.R"

rule inputs2test:
    """Prepare simple inputs for computPGS to improve efficiency"""
    input:
        bgen=config['geno_dir'] + "/ukb_imp_chr1_v3.bgen",
        pgs=config['output_dir'] + "/model_QC/pgs_ukbb_T2D_Scott_28566273_1_qcfilt_RapidoPGSsingle_prior02.full.model",
        sample_bgen=config['pheno_dir'] + "/samplefiles/ukb30931_imp_chr1_v3_s487296.sample"
    params:
        genome_build=37,
        chunk=100,
        cols=["CHR", "BP", "REF", "ALT", "rsid", "weight"],
        chrom=1
    wildcard_constraints:
        chrom="\d+",
        breaks="\d+"
    output:
        gt=config['output_dir'] +  "/input_test/T2D_Scott_28566273_1_qcfilt_RapidoPGSsingle_prior02.full.model.chrom1.100var.rds",
        snp_info=config['output_dir'] +  "/input_test/T2D_Scott_28566273_1_qcfilt_RapidoPGSsingle_prior02.full.model.chrom1.100var.info.txt"
    script:
        "Scripts/inputs_test.R"
        

# rule merge_scores:
#     """After computing scores by chrom chunks add sub-scores into one file. I do the merging in python. """
#     input:
#         scores=lambda wildcards: [config['out_tmp'] + "/ukbb_score/" + wildcards.model + "/" + wildcards.model + ".{chrom}.{breaks}.txt".format(chrom=str(bk), breaks=str(v)) for bk 
# in break_chrom(d=pgs_dic_counts["pgs_ukbb_" + wildcards.model],b= 1000).keys() for v in break_chrom(d=pgs_dic_counts["pgs_ukbb_" + wildcards.model], b=1000)[bk]]
#     wildcard_constraints:
#         chrom="\d+",
#         breaks="\d+"
#     output:
#         scores=config['out_tmp'] + "/ukbb_score/{model}/{model}.total.txt"
#     script:
#        home_covid + "/Scripts/merge_ld2scores.py"


rule subset_score:
    """Select relevant individuals to report score, same as in ld2pred paper, https://github.com/privefl/simus-PRS/blob/master/paper3-SCT/code_real/"""
    input:
        score=config['out_tmp'] + "/ukbb_score/{model}/{model}.total.txt",
        rel_ind=config["pheno_dir"] + "/keyfile/ukb30931_rel_s488264.dat",
        eid=config['eid_pheno']
    params:
        trait=lambda wildcards: re.sub('[a-z].*', '', wildcards.model.split("_")[0])
    output:
        score=config['out_tmp'] + "/ukbb_score/ind_subset/{model}.score.txt"
    script:
        "Scripts/{params.trait}.subset.R"
        

rule report_auc_r2:
    """Compare scores by relevant measure and make a report including a QC step of discarded SNPs. Outpur file with report and table with model and auc or r2"""
    input:
        excluded=expand(config['output_dir'] + "/model_QC/failed_{model}", model=model_files),
        kept=expand(config['output_dir'] + "/model_QC/pgs_ukbb_{model}", model=model_files),
        scores=expand(config['out_tmp'] + "/ukbb_score/ind_subset/{model}.score.txt", model=model_files),
        script="Scripts/report_scores.R"
    params:
        model=model_files,
        out_dir=config['output_dir'] + "/auc_r2"
    output:
        "Scripts/report_scores.pdf"
    script:
        home_covid + "/Scripts/RenderReport.R"
    
   

        

## snakemake  -k -j 1000 --cluster-config cpu.json --cluster "sbatch -A {cluster.account} -p {cluster.partition}  -c {cluster.cpus-per-task}   -t {cluster.time} --output {cluster.error} -J {cluster.job} "

## snakemake  -k -j 500 --cluster-config himem.json --cluster "sbatch -A {cluster.account} -p {cluster.partition}  -c {cluster.cpus-per-task}   -t {cluster.time} --output {cluster.error} -J {cluster.job} "
