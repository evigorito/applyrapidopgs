
<!-- README.md is generated from README.Rmd. Please edit that file -->

## applyrapidoPGS pipeline

This pipeline is designed to applied pre-built PGS to selected samples
of UKBB.

## Requirements

R versions \>= 3.4.0.

### To clone the applyrapidoPGS repository do:

``` bash
# clone the applyPGS repository
 git clone https://gitlab.com/evigorito/applyrapidopgs.git
```

### To install the R libraries required for the pipeline do:

``` r
## Within R:
install.packages( "http://www.well.ox.ac.uk/~gav/resources/rbgen_v1.1.4.tgz", repos = NULL, type = "source" )

install.packages("devtools") # if you don't already have the package

library(devtools)
devtools::install_gitlab("evigorito/runpgs")
```

# Workflow

The
[Snakefile](https://gitlab.com/evigorito/applyrapidopgs/-/blob/master/Snakefile)
details the steps performed for analysis. The config.yaml file is not
inlcuded in the repo because contains paths to data directories. For
each PGS score an initial QC is performed checking if the SNPs in the
PGS are present in UKBB. Multiallelic SNPs or SNPs below a user defined
info score are discarded (rule score\_qc).

For the SNPs that passed the QC the PGS is applied to UKBB data (rules
comput\_PGS and merge\_scores). Relevant individuals to apply the score
are selected by rule subset\_score and finally a report summarising QC
and calculating predictive measure of the score is produced by rule
report\_auc\_r2.
